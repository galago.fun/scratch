package main

import (
    "fmt"
    "net/http"
)

func main() {
    http.HandleFunc("/", YoServer)
    http.ListenAndServe(":8080", nil)
}

func YoServer(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Yo, %s!", r.URL.Path[1:])
}
